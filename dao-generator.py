import ddlparse
import jinja2
import argparse
import os
import sys
import ast


def to_lower_camel_case(snake_str):
    components = snake_str.split('_')
    return components[0] + ''.join(x.title() for x in components[1:])


def to_camel_case(snake_str):
    components = snake_str.split('_')
    return ''.join(x.title() for x in components)


def setup_args():
    parser = argparse.ArgumentParser(description='Converts a DDL to corresponding POJO and DAO')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--ddl', help='Pass DDL as argument in double-quotes')
    group.add_argument('--ddlpath', help='Pass file path containing DDL (No double-quotes)')
    parser.add_argument('-t', '--typemappings', help='path to file which stores DB to Java datatype mapping', required=False)
    parser.add_argument('-v', '--verbose', help='To print more logs', required=False, action='store_true')
    parser.add_argument('-o', '--output', help='Path to directory where output files should be generated', required=False)
    return parser.parse_args()


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


def get_java_data_mapping():

    ret_val = {}
    type_mapping_dict = {}
    if type_mapping_path:
        if os.path.exists(type_mapping_path):
            with open(type_mapping_path, 'r') as type_mapping_file:
                try:
                    type_mapping_dict.update(ast.literal_eval(type_mapping_file.read()))
                except SyntaxError:
                    print("Invalid python syntax in type-mapping file. Exiting.")
                    sys.exit(1)
    else:
        import type_mappings
        type_mapping_dict = type_mappings.TYPES
    for k, v in type_mapping_dict.items():
        for key in k:
            ret_val[key] = v
    return ret_val


def get_parsed_ddl(ddl):
    try:
        ret_val = ddlparse.DdlParse().parse(ddl=ddl, source_database=ddlparse.DdlParse.DATABASE.postgresql)
        return ret_val
    except:
        print("Error parsing DDL. Exiting.")
        sys.exit(1)


def get_attribute_list(table, java_data_types):
    ret_val = []
    for col in table.columns.values():
        attr_name = to_lower_camel_case(col.name)
        if col.data_type not in java_data_types:
            print("Please add mapping for Java datatype for {}".format(col.data_type))
            sys.exit(1)
        attr_type = java_data_types[col.data_type]
        ret_val.append('private {} {};'.format(attr_type, attr_name))
    return ret_val


def print_file(content, file_name, output_path_dir):
    if not os.path.exists(output_path_dir):
        print("Please provide valid output path")
        sys.exit(1)
    file_path = os.path.join(output_path_dir, file_name)
    with open(file_path, 'w') as output_file:
        output_file.write(content)


def generate_pojo(ddl):

    parsed_table = get_parsed_ddl(ddl=ddl)
    if not parsed_table.columns.values():
        print("No columns in DDL. Please pass valid DDLs.")
        sys.exit(1)

    if verbose:
        for col in parsed_table.columns.values():
            print("{}: {}".format(col.name, col.data_type))

    class_name = to_camel_case(parsed_table.name)
    pojo_class_variable = to_lower_camel_case(parsed_table.name)
    java_data_types = get_java_data_mapping()
    attribute_list = get_attribute_list(table=parsed_table, java_data_types=java_data_types)

    pojo_template_path = resource_path('pojo_template')
    with open(pojo_template_path, mode='r', encoding='utf8') as dao_template_file:
        data = dao_template_file.read()

    pojo_template = jinja2.Template(data)
    java_class = pojo_template.render(class_name=class_name, attribute_list=attribute_list)

    if verbose:
        print("Generated Java class:")
        print(java_class)

    pojo_file_name = "{}.java".format(class_name)

    print_file(content=java_class, file_name=pojo_file_name, output_path_dir=output_path)

    dao_class_name = class_name + 'Dao'
    jooq_table_name = parsed_table.name.upper()
    jooq_set_row_list = []
    fetch_db_pk = ''
    delete_db_pk = ''
    fetch_condition = ''
    update_pojo_pk_getter = ''
    update_condition = ''
    delete_condition = ''
    for col in parsed_table.columns.values():
        query_key = parsed_table.name.upper() + '.' + col.name.upper()
        query_val = pojo_class_variable + '.get' + to_camel_case(col.name) + '()'

        if col.primary_key:
            fetch_db_pk = to_lower_camel_case(col.name)
            delete_db_pk = fetch_db_pk
            update_pojo_pk_getter = query_val
            fetch_condition = query_key + '.eq(' + to_lower_camel_case(col.name) + ')'
            delete_condition = query_key + '.eq(' + to_lower_camel_case(col.name) + ')'
            update_condition = query_key + '.eq(' + pojo_class_variable + '.get' + to_camel_case(col.name) + '())'
        else:
            set_stmnt = ".set("
            set_stmnt += query_key
            set_stmnt += ", "
            java_data_type = java_data_types[col.data_type]
            if java_data_type == 'Date':
                set_stmnt += 'timestamp('
                set_stmnt += query_val
                set_stmnt += ')'
            else:
                set_stmnt += query_val
            set_stmnt += ')'
            jooq_set_row_list.append(set_stmnt)

    dao_context = {
        'dao_class_name': dao_class_name,
        'pojo_class_name': class_name,
        'pojo_class_variable': pojo_class_variable,
        'jooq_table_name': jooq_table_name,
        'jooq_set_row_list': jooq_set_row_list,
        'fetch_db_pk': fetch_db_pk,
        'fetch_condition': fetch_condition,
        'update_condition': update_condition,
        'update_pojo_pk_getter': update_pojo_pk_getter,
        'delete_db_pk': delete_db_pk,
        'delete_condition': delete_condition,
    }

    dao_template_path = resource_path('dao_template')
    with open(dao_template_path, mode='r', encoding='utf8') as dao_template_file:
        data = dao_template_file.read()

    dao_template = jinja2.Template(data)
    dao_class = dao_template.render(dao_context=dao_context)
    if verbose:
        print(dao_class)

    dao_class_file_name = '{}.java'.format(dao_class_name)
    print_file(content=dao_class, file_name=dao_class_file_name, output_path_dir=output_path)


def generate_dao(ddl):
    generate_pojo(ddl=ddl)


if __name__ == '__main__':
    args = setup_args()
    ddl_arg = args.ddl
    ddl_path = args.ddlpath
    verbose = args.verbose
    output_path = args.output
    type_mapping_path = args.typemappings
    if not output_path:
        output_path = os.getcwd()
    if not ddl_arg:
        if not os.path.exists(ddl_path):
            print("Please provide valid DDL or DDL location")
        with open(ddl_path, 'r') as ddl_input_file:
            ddl_arg = ddl_input_file.read().strip()
            if ddl_arg:
                ddl_arg = ' '.join([s.strip() for s in ddl_arg.split('\n')])

    if verbose:
        print("Received DDL: {}".format(ddl_arg))
    generate_dao(ddl=ddl_arg)
