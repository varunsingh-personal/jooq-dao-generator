DDL Generator
Takes a DDL statement and generates corresponding POJO and DAO class.

Usage: dao-generator [-h] (--ddl DDL | --ddlpath DDLPATH) [-t TYPEMAPPINGS] [-v] [-o OUTPUT]

-h, --help            show this help message and exit
--ddl DDL             Pass DDL as argument in double-quotes
--ddlpath DDLPATH     Pass file path containing DDL (No double-quotes)
-v, --verbose         To print more logs
-o OUTPUT, --output OUTPUT
                      Path to directory where output files should be
                        generated

Example: ./dao-generator --ddlpath ../test-ddl.sql -o . -t type-mappings.txt


